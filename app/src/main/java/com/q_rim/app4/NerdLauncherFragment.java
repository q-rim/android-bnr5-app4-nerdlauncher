package com.q_rim.app4;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NerdLauncherFragment extends ListFragment{
  private static final String TAG = "---NerdLauncherFragment";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Querying the PackageManager <23.2>
    Intent startupIntent = new Intent(Intent.ACTION_MAIN);
    startupIntent.addCategory(Intent.CATEGORY_LAUNCHER);

    PackageManager pm = getActivity().getPackageManager();
    List<ResolveInfo> activities = pm.queryIntentActivities(startupIntent, 0);

    Log.i(TAG, "Found: " + activities.size() + " activities.");

    // Sorting ResolveInfo Objects alphabetically
    Collections.sort(activities, new Comparator<ResolveInfo>() {
      @Override
      public int compare(ResolveInfo lhs, ResolveInfo rhs) {
        PackageManager pm = getActivity().getPackageManager();
        return String.CASE_INSENSITIVE_ORDER.compare(lhs.loadLabel(pm).toString(), rhs.loadLabel(pm).toString());
      }
    });

    // Creating a ListView of Activities:  Create an Adapter <23.4>
    ArrayAdapter<ResolveInfo> adapter = new ArrayAdapter<ResolveInfo>(getActivity(), android.R.layout.simple_list_item_1, activities) {
      public View getView(int pos, View convertView, ViewGroup parent) {
        PackageManager pm = getActivity().getPackageManager();
        View v = super.getView(pos, convertView, parent);
        // Documentatino says that the simple_list_item_1 is a TextView,
        // so cast it so that you can set its text value.
        TextView tv = (TextView)v;
        ResolveInfo ri = getItem(pos);
        tv.setText(ri.loadLabel(pm));
        return v;
      }
    };
    setListAdapter(adapter);
  }

  // Impliment onListItemClick <23.5>
  // clicking the listItem will now launch the Application's Activity.
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    ResolveInfo resolveInfo = (ResolveInfo)l.getAdapter().getItem(position);
    ActivityInfo activityInfo = resolveInfo.activityInfo;

    if (activityInfo == null) return;
    Intent i = new Intent(Intent.ACTION_MAIN);
    i.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
    // Criminal Intent start it's own Activity. Add new task flag to intent <23.6>
    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(i);
  }
}
